# Library Manager

Notre service est une API permetant de gérer des bibliothèques. Celles-ci contiennent des livres, qui peuvent être empruntés par des utilisateurs, inscrit dans une bibliothèque.

## Documentation
La documentation se trouve dans le dossier [Docs](https://gitlab.iut-clermont.uca.fr/gabrugiere/library-management/-/tree/master/docs), ainsi que dans le [wiki](https://gitlab.iut-clermont.uca.fr/gabrugiere/library-management/-/wikis) ([MCD](https://gitlab.iut-clermont.uca.fr/gabrugiere/library-management/-/wikis/MCD), [UML](https://gitlab.iut-clermont.uca.fr/gabrugiere/library-management/-/wikis/UML)).\
La JavaDoc se trouve dans le dossier [docs/Javadoc](https://gitlab.iut-clermont.uca.fr/gabrugiere/library-management/-/tree/master/docs/Javadoc).

## Service
Le service est découpé en 4 sous-services permettant de gérer indépendament les entités des bibliothèques :
- Bibliothèque (Library) ;
- Utilisateur (User) ;
- Livre (Book) ;
- Auteur (Author).

### Bibliothèque
Le service de bibliothèques permet de gérer les bibliothèques voulant rejoindre le projet.\
Il est possible de récupérer la liste des bibliothèques, de récupérer une bibliothèque à l'aide de son ID, d'ajouter une bibliothèque, et enfin de modifier une bibliothèque (à l'aide de son ID).\
Une bibliothèque contient un nom, et une adresse, sous forme de sous document.

### Utilisateur
Le service des utilisateurs permet de gérer les utilisateurs des bibliothèques (un utilisateur peut appartenir à plusieurs bibliothèques) et leurs emprunts.\
Il est possible de lister tous les utilisateurs. On peut aussi récupérer un utilisateur par son ID, le supprimer ou le modifier, et en ajouter.\
On peut aussi ajouter une bibliothèque à laquelle appartient l'utilisateur.\
L'utilisateur contient un nom, un prénom, un àge et les bibliothèques à qui il appartient.

### Livre
Le service des livres permet de gérer les livres au seins des bibliothèques et leurs emprunts.\
De la même manière, il est possible de lister tous les livres, ou bien en récupérer un par son ISBN, de le modifier ou le supprimer et en ajouter un.\
De plus, nous pouvons ajouter les utilisateurs qui emprunte un livre et enfin ajouter des opinions.\
Un livre contient donc un ISBN, un titre, un (des) auteur(s) (dans une liste, contenant les ID des auteurs, cf service des auteurs), un résumé, une liste d'opinion, une liste de bibliothèque possédant le livre (pour chaque bibliothèque, on retrouve son ID et la quantité) et enfin la liste des utilisateurs qui ont empruntés les livres.\

### Auteur
Enfin, le service de gestion des auteurs permet de voir la liste des auteurs, d'obtenir un auteur à l'aide de son ID, de le supprimer, le modifier ou d'en ajouter un.\
Un auteur contient son nom, son prénom et sa biographie.
