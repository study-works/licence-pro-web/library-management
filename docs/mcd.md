![MCD_Library_Management.svg](uploads/8ab5e53b4c58120420cfe442fb199f72/MCD_Library_Management.svg)
MCD du projet Library Management

## Les besoins couverts
- L'application étant une application de gestion de bibliothèques, on retrouve une entité pour représenter les bibliothèques utilisant l'application. Une bibliothèque a une adresse, d'où l'entité.
- Les livres, appartenant à une bibliothèque, ont leur propre entité, permettant de les décrire, et de leur rajouter un (ou des) auteur(s), ainsi que des opinions.
- On retrouve enfin une entité représentant les utilisateurs, appartenant à une (ou des) bibliothèque(s) et qui peuvent emprunter un (des) livre(s).
