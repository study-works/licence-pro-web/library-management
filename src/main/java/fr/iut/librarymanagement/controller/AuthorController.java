package fr.iut.librarymanagement.controller;

import fr.iut.librarymanagement.controller.exception.EmptyException;
import fr.iut.librarymanagement.controller.exception.NotFoundException;
import fr.iut.librarymanagement.domain.Author;
import fr.iut.librarymanagement.repository.AuthorRepository;
import org.bson.types.ObjectId;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * Controller for authors
 *
 * @see Author
 */
@RestController
public class AuthorController {

    /**
     * The AuthorRepository
     */
    private final AuthorRepository authorRepository;

    /**
     * @param authorRepository The AuthorRepository
     */
    public AuthorController(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    /**
     * Get all authors, or looking for all authors by firstname or lastname
     *
     * @param lastname To looking for by lastname
     * @param firstname To looking for by firstname
     * @return A list of Author
     */
    @GetMapping(path = "/authors", produces = "Application/json")
    public List<Author> authorList(@RequestParam(name = "lastname", required = false) String lastname, @RequestParam(name = "firstname", required = false) String firstname){
        if(lastname != null){
            return authorRepository.findByLastnameOrderByLastnameAsc(lastname);
        } else if (firstname != null){
            return authorRepository.findByFirstnameOrderByLastnameAsc(firstname);
        }
        return authorRepository.findAll();
    }

    /**
     * Get an author by its ID
     *
     * @param id The ID of the author
     * @return The author
     */
    @GetMapping(path = "/author/{id}", produces = "Application/json")
    public Optional<Author> getAuthorById(@PathVariable ObjectId id){
        Optional<Author> author = authorRepository.findById(id);

        if (author.isEmpty()) {
            throw new NotFoundException("Author not found!");
        }

        return author;
    }

    /**
     * Create an author
     *
     * @param author The author to be created
     * @return The new author
     */
    @PostMapping(path = "/author", consumes = "Application/json")
    public Author addAuthor(@RequestBody Author author){
        if (author == null){
            throw new EmptyException("Author can't be null");
        }
        return authorRepository.save(author);
    }

    /**
     * Update an author
     *
     * @param id The ID of the author to be modified
     * @param lastname  The lastname of the author
     * @param firstname The firstname of the author
     * @return The modified author
     */
    @PutMapping(path = "/author/{id}")
    public Author editAuthor(@PathVariable ObjectId id, @RequestParam(name = "lastname", required = false) String lastname, @RequestParam(name = "firstname", required = false) String firstname){
        Optional<Author> author = authorRepository.findById(id);

        if (author.isEmpty()) {
            throw new NotFoundException("Author not found!");
        }

        if(lastname != null){
            author.get().setLastname(lastname);
        }
        if (firstname != null){
            author.get().setFirstname(firstname);
        }
        return authorRepository.save(author.get());
    }


}
