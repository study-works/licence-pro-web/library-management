package fr.iut.librarymanagement.controller.exception;

/**
 * Exception to be thrown if the parameter of a request is empty when it should not be
 */
public class EmptyException extends RuntimeException {

    /**
     * @param message The message to be displayed
     */
    public EmptyException(String message) {
        super(message);
    }
}
