package fr.iut.librarymanagement.controller.exception;

/**
 * Exception to be thrown if a requested element of the database is not found
 */
public class NotFoundException extends RuntimeException {

    /**
     * @param message The message to be displayed
     */
    public NotFoundException(String message) {
        super(message);
    }
}
