package fr.iut.librarymanagement.controller.exception;

/**
 * Exception to be lifted if there is a problem with the rating of an opinion
 */
public class RatingException extends RuntimeException {

    /**
     * @param message The message to be displayed
     */
    public RatingException(String message) {
        super(message);
    }
}
