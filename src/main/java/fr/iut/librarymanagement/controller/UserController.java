package fr.iut.librarymanagement.controller;

import fr.iut.librarymanagement.controller.exception.EmptyException;
import fr.iut.librarymanagement.controller.exception.NotFoundException;
import fr.iut.librarymanagement.domain.User;
import fr.iut.librarymanagement.repository.UserRepository;
import org.bson.types.ObjectId;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * Controller for users
 *
 * @see User
 */
@RestController
public class UserController {

    /**
     * The UserRepository
     */
    private final UserRepository userRepository;

    /**
     * @param userRepository The UserRepository
     */
    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Get all users, or looking for all users by firstname or lastname
     *
     * @param lastname To looking for by lastname
     * @param firstname To looking for by firstname
     * @return A list of User
     */
    @GetMapping(path = "/users", produces = "Application/json")
    public List<User> userList(@RequestParam(name = "lastname", required = false) String lastname, @RequestParam(name = "firstname", required = false) String firstname){
        if(lastname != null){
            return userRepository.findByLastnameOrderByLastnameAsc(lastname);
        } else if (firstname != null){
            return userRepository.findByFirstnameOrderByLastnameAsc(firstname);
        }
        return userRepository.findAll();
    }

    /**
     * Get an user by its ID
     *
     * @param id The ID of the user
     * @return The user
     */
    @GetMapping(path = "/user/{id}", produces = "Application/json")
    public Optional<User> getUserById(@PathVariable ObjectId id){
        Optional<User> user = userRepository.findById(id);

        if (user.isEmpty()) {
            throw new NotFoundException("User not found!");
        }

        return user;
    }

    /**
     * Create an user
     *
     * @param user The user to be created
     * @return The new user
     */
    @PostMapping(path = "/user", consumes = "Application/json")
    public User addUser(@RequestBody User user){
        if (user == null){
            throw new EmptyException("User can't be null");
        }
        return userRepository.save(user);
    }

    /**
     * Delete a user
     *
     * @param id The ID of the user to be deleted
     */
    @DeleteMapping(path = "/user/{id}")
    public void deleteUser(@PathVariable ObjectId id){
        userRepository.deleteById(id);
    }

    /**
     * Update an user
     *
     * @param id The ID of the user to be modified
     * @param lastname  The lastname of the user
     * @param firstname The firstname of the user
     * @return The modified user
     */
    @PutMapping(path = "/user/{id}")
    public User editUser(@PathVariable ObjectId id, @RequestParam(name = "lastname", required = false) String lastname, @RequestParam(name = "firstname", required = false) String firstname){
        Optional<User> user = userRepository.findById(id);

        if (user.isEmpty()) {
            throw new NotFoundException("User not found!");
        }

        if(lastname != null){
            user.get().setLastname(lastname);
        }
        if (firstname != null){
            user.get().setFirstname(firstname);
        }
        return userRepository.save(user.get());
    }

    /**
     * Add a library where a user is
     *
     * @param id The IS of the user
     * @param libraryId The ID of the library
     * @return The user with its new library
     */
    @PostMapping(path = "/user/{id}/library")
    public User addUser(@PathVariable ObjectId id, @RequestBody ObjectId libraryId){
        Optional<User> user = userRepository.findById(id);

        if (user.isEmpty()) {
            throw new NotFoundException("User not found!");
        }

        user.get().addLibrary(libraryId);
        return userRepository.save(user.get());
    }

}
