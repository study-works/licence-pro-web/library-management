package fr.iut.librarymanagement.controller;

import fr.iut.librarymanagement.controller.exception.EmptyException;
import fr.iut.librarymanagement.controller.exception.NotFoundException;
import fr.iut.librarymanagement.domain.Library;
import fr.iut.librarymanagement.repository.LibraryRepository;
import org.bson.types.ObjectId;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * Controller for libraries
 *
 * @see Library
 */
@RestController
public class LibraryController {

    /**
     * The LibraryRepository
     */
    private final LibraryRepository libraryRepository;

    /**
     * @param libraryRepository The LibraryRepository
     */
    public LibraryController(LibraryRepository libraryRepository) {
        this.libraryRepository = libraryRepository;
    }

    /**
     * Get all libraries, or looking for all libraries by name or city
     *
     * @param name To looking for by name
     * @param city To looking fot by city
     * @return A list of library
     */
    @GetMapping(path = "/libraries", produces = "Application/json")
    public List<Library> libraryList(@RequestParam(name = "name", required = false) String name, @RequestParam(name = "city", required = false) String city){
        if(name != null){
            return libraryRepository.findByName(name);
        } else if (city != null){
            return libraryRepository.findByAddress_City(city);
        }
        return libraryRepository.findAll();
    }

    /**
     * Get a library by its ID
     *
     * @param id The ID of the library
     * @return The library
     */
    @GetMapping(path = "/library/{id}", produces = "Application/json")
    public Optional<Library> getLibraryById(@PathVariable ObjectId id){
        Optional<Library> library = libraryRepository.findById(id);

        if (library.isEmpty()) {
            throw new NotFoundException("Library not found!");
        }

        return library;
    }

    /**
     * Create an library
     *
     * @param library The library to be created
     * @return The new library
     */
    @PostMapping(path = "/library", consumes = "Application/json")
    public Library addLibrary(@RequestBody Library library){
        if (library == null){
            throw new EmptyException("Library can't be null");
        }
        return libraryRepository.save(library);
    }

    /**
     * Update a library
     *
     * @param id The ID of the library to be modified
     * @param address The address of the library
     * @param name The name of the library
     * @return The modified library
     */
    @PutMapping(path = "/library/{id}")
    public Library editLibrary(@PathVariable ObjectId id, @RequestParam(name = "address", required = false) String address, @RequestParam(name = "name", required = false) String name){
        Optional<Library> library = libraryRepository.findById(id);

        if (library.isEmpty()) {
            throw new NotFoundException("Library not found!");
        }

        if(address != null){
            library.get().getAddress().setAddress(address);
        }
        if (name != null){
            library.get().setName(name);
        }
        return libraryRepository.save(library.get());
    }
}
