package fr.iut.librarymanagement.controller.advice;

import fr.iut.librarymanagement.controller.exception.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Advice for NotFoundException
 * @see NotFoundException
 */
@ControllerAdvice
public class NotFoundExceptionAdvice {

    /**
     * @param ex The exception
     * @return The message
     */
    @ResponseBody
    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String notFoundExceptionHandler(NotFoundException ex) {
        return ex.getMessage();
    }
}
