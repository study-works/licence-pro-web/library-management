package fr.iut.librarymanagement.controller.advice;

import fr.iut.librarymanagement.controller.exception.RatingException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Advice for RatingException
 * @see RatingException
 */
@ControllerAdvice
public class RatingExceptionAdvice {

    /**
     * @param ex The exception
     * @return The message
     */
    @ResponseBody
    @ExceptionHandler(RatingException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    String ratingExceptionHandler(RatingException ex) {
        return ex.getMessage();
    }
}
