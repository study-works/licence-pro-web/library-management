package fr.iut.librarymanagement.controller.advice;

import fr.iut.librarymanagement.controller.exception.EmptyException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Advice for EmptyException
 * @see EmptyException
 */
@ControllerAdvice
public class EmptyExceptionAdvice {

    /**
     * @param ex The exception
     * @return The message
     */
    @ResponseBody
    @ExceptionHandler(EmptyException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    String emptyExceptionHandler(EmptyException ex) {
        return ex.getMessage();
    }
}
