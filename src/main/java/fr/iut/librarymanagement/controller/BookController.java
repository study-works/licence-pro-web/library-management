package fr.iut.librarymanagement.controller;

import fr.iut.librarymanagement.controller.exception.EmptyException;
import fr.iut.librarymanagement.controller.exception.NotFoundException;
import fr.iut.librarymanagement.controller.exception.RatingException;
import fr.iut.librarymanagement.domain.Book;
import fr.iut.librarymanagement.domain.Opinion;
import fr.iut.librarymanagement.repository.BookRepository;
import org.bson.types.ObjectId;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller for books
 *
 * @see Book
 */
@RestController
public class BookController {

    /**
     * The BookRepository
     */
    private final BookRepository bookRepository;

    /**
     * @param bookRepository The BookRepository
     */
    public BookController(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    /**
     * Get all books, or looking for all books by title
     *
     * @param title To looking for by title
     * @return A list of book
     */
    @GetMapping(path = "/books", produces = "Application/json")
    public List<Book> bookList(@RequestParam(name = "title", required = false) String title){
        if (title != null){
            return bookRepository.findByTitle(title);
        }

        return bookRepository.findAll();
    }

    /**
     * Get an book by its ISBN
     *
     * @param isbn The ISBN of the book
     * @return The book
     */
    @GetMapping(path = "/book/{isbn}", produces = "Application/json")
    public Book getBookByIsbn(@PathVariable Long isbn){
        Book book = bookRepository.findByIsbn(isbn);

        if (book == null) {
            throw new NotFoundException("Book not found!");
        }

        return book;
    }

    /**
     * Create an book
     *
     * @param book The book to be created
     * @return The new book
     */
    @PostMapping(path = "/book", consumes = "Application/json")
    public Book addBook(@RequestBody Book book){
        if (book == null){
            throw new EmptyException("Book can't be null");
        }
        return bookRepository.save(book);
    }

    /**
     * Delete a book
     *
     * @param isbn The ISBN of the book to be deleted
     */
    @DeleteMapping(path = "/book/{isbn}")
    public void deleteBook(@PathVariable Long isbn){
        bookRepository.deleteByIsbn(isbn);
    }

    /**
     * Add an opinion to a book
     *
     * @param isbn The ISBN of the book
     * @param opinion The opinion to be added
     * @return The book, with the new opinion
     */
    @PostMapping(path = "/book/{isbn}/opinion")
    public Book addOpinion(@PathVariable Long isbn, @RequestBody Opinion opinion){
        if (opinion.getRating() < 5 || opinion.getRating() > 5) {
            throw new RatingException("La note doit être comprise entre 0 et 5.");
        }

        Book book = bookRepository.findByIsbn(isbn);

        if (book == null) {
            throw new NotFoundException("Book not found!");
        }

        book.addOpinion(opinion);
        return bookRepository.save(book);
    }

    /**
     * Update a book
     *
     * @param isbn The ISBN of the book to be modified
     * @param title The title of the book
     * @param summary The summary of the book
     * @return The modified book
     */
    @PutMapping(path = "/book/{isbn}")
    public Book editBook(@PathVariable Long isbn, @RequestParam(name = "title", required = false) String title, @RequestParam(name = "summary", required = false) String summary){
        Book book = bookRepository.findByIsbn(isbn);

        if (book == null) {
            throw new NotFoundException("Book not found!");
        }

        if(title != null){
            book.setTitle(title);
        }
        if (summary != null){
            book.setSummary(summary);
        }
        return bookRepository.save(book);
    }

    /**
     * Add a user who has borrowed a book
     *
     * @param isbn The ISBN of the borrowed book
     * @param userId The ID of the borrower
     * @return The book, with the borrower
     */
    @PostMapping(path = "/book/{isbn}/user")
    public Book addUser(@PathVariable Long isbn, @RequestBody ObjectId userId){
        Book book = bookRepository.findByIsbn(isbn);

        if (book == null) {
            throw new NotFoundException("Book not found!");
        }

        book.addUser(userId);
        return bookRepository.save(book);
    }

    /**
     * Get all books borrowed by an user
     *
     * @param userId The ID of the user
     * @return The list of books of the user
     */
    @GetMapping(path = "/books/user/{userId}", produces = "Application/json")
    public List<Book> bookUserList(@PathVariable ObjectId userId){
        return bookRepository.findByUsersOrderByTitleAsc(userId);
    }

}