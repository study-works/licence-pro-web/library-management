package fr.iut.librarymanagement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;

/**
 * The main application
 */
@SpringBootApplication
public class LibraryManagementApplication {

    /**
     * Allows to log
     */
    static Logger logger = LoggerFactory.getLogger(LibraryManagementApplication.class);

    /**
     * The main to launch the app
     * @param args Different args
     */
    public static void main(String[] args) {
        SpringApplication.run(LibraryManagementApplication.class, args);
        logger.info("Message= \uD83D\uDE80 Application started");
    }

    /**
     * Overrides the MongoDB Spring client to add automatic index creation
     */
    @Bean
    public MongoTemplate mongoTemplate(MongoDatabaseFactory mongoDbFactory, MongoMappingContext context) {
        MappingMongoConverter converter = new MappingMongoConverter(new DefaultDbRefResolver(mongoDbFactory), context);
        context.setAutoIndexCreation(true);
        return new MongoTemplate(mongoDbFactory, converter);
    }

}
