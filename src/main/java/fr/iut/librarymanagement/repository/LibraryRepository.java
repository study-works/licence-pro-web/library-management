package fr.iut.librarymanagement.repository;

import fr.iut.librarymanagement.domain.Library;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * The repository of the Author
 *
 * @see Library
 */
public interface LibraryRepository extends MongoRepository<Library, ObjectId> {
    /**
     * Find libraries by name
     *
     * @param name The name to be looking for
     * @return A list of libraries
     */
    List<Library> findByName(String name);

    /**
     * Find libraries by city
     *
     * @param city The city to be looking for
     * @return A list of libraries
     */
    List<Library> findByAddress_City(String city);
}
