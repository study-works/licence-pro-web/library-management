package fr.iut.librarymanagement.repository;

import fr.iut.librarymanagement.domain.Book;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * The repository of the Author
 *
 * @see Book
 */
public interface BookRepository extends MongoRepository<Book, ObjectId> {
    /**
     * Find a book with an ISBN
     *
     * @param isbn The ISBN
     * @return The book
     */
    Book findByIsbn(Long isbn);

    /**
     * Find books with a title
     *
     * @param title The title to be looking for
     * @return A list of book
     */
    List<Book> findByTitle(String title);

    /**
     * Delete a book with its ISBN
     *
     * @param isbn The ISBN of the book to be deleted
     * @return The deleted book
     */
    Book deleteByIsbn(Long isbn);

    /**
     * Find all books borrowed by an user
     *
     * @param id The ID of the user
     * @return The list of book of a user
     */
    List<Book> findByUsersOrderByTitleAsc(ObjectId id);
}
