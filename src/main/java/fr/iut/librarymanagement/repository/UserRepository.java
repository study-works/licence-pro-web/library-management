package fr.iut.librarymanagement.repository;

import fr.iut.librarymanagement.domain.User;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * The repository of the Author
 *
 * @see User
 */
public interface UserRepository extends MongoRepository<User, ObjectId> {
    /**
     * Get users by there lastname
     *
     * @param lastname The lastname
     * @return A list of user
     */
    List<User> findByLastnameOrderByLastnameAsc(String lastname);

    /**
     * Get users by there firstname
     *
     * @param firstname The firstname
     * @return A list of user
     */
    List<User> findByFirstnameOrderByLastnameAsc(String firstname);
}
