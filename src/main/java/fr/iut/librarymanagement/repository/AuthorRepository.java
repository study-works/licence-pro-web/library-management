package fr.iut.librarymanagement.repository;

import fr.iut.librarymanagement.domain.Author;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

/**
 * The repository of the Author
 *
 * @see Author
 */
public interface AuthorRepository extends MongoRepository<Author, ObjectId> {
    /**
     * Get authors by there lastname
     *
     * @param lastname The lastname
     * @return A list of author
     */
    List<Author> findByLastnameOrderByLastnameAsc(String lastname);

    /**
     * Get authors by there lastname
     *
     * @param firstname The firstname
     * @return A list of author
     */
    List<Author> findByFirstnameOrderByLastnameAsc(String firstname);

    /**
     * Get an author by its ID
     *
     * @param id The ID
     * @return An author
     */
    Optional<Author> findById(ObjectId id);
}
