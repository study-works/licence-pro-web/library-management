package fr.iut.librarymanagement.domain;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

/**
 * Represent the opinion of a book
 * @see Book
 */
public class Opinion {
    private String pseudo;
    private Double rating;
    private String comment;

    public Opinion() {
    }

    /**
     * Create the opinion
     *
     * @param pseudo The pseudo of the person who post the opinion (not a User in base)
     * @param rating The rating give by the person (0 <= rating <= 5
     * @param comment The comment left by the person
     */
    public Opinion(String pseudo, Double rating, String comment) {
        this.pseudo = pseudo;
        this.rating = rating;
        this.comment = comment;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
