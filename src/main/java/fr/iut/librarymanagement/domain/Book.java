package fr.iut.librarymanagement.domain;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Represent a book in base
 */
@Document(collection = "Book")
public class Book {
    /**
     * The ID of the user. Automatically generated
     */
    @Id
    private ObjectId id;
    private Long isbn;
    private String title;
    private List<ObjectId> authors;
    private String summary;
    private List<Opinion> opinions = new ArrayList<>();
    private List<LibraryQuantityAssociation> libraryQuantityAssociations;
    private List<ObjectId> users = new ArrayList<>();

    public Book() {
    }

    /**
     * Create a book
     *
     * @param isbn The ISBN of the book (ex: 2267027003 for Le seigneur des anneaux T.1)
     * @param title The title (ex: Le seigneur des anneaux T.1)
     * @param authors The author(s) of a book
     * @param summary The summary of a book
     * @param libraryQuantityAssociations The library with its quantity {@link LibraryQuantityAssociation}
     */
    public Book(Long isbn, String title, List<ObjectId> authors, String summary, List<LibraryQuantityAssociation> libraryQuantityAssociations) {
        this.isbn = isbn;
        this.title = title;
        this.authors = authors;
        this.summary = summary;
        this.libraryQuantityAssociations = libraryQuantityAssociations;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public Long getIsbn() {
        return isbn;
    }

    public void setIsbn(Long isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<ObjectId> getAuthors() {
        return authors;
    }

    public void setAuthors(List<ObjectId> authors) {
        this.authors = authors;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public List<Opinion> getOpinions() {
        return opinions;
    }

    public void setOpinions(List<Opinion> opinions) {
        this.opinions = opinions;
    }

    public List<LibraryQuantityAssociation> getLibraryQuantityAssociations() {
        return libraryQuantityAssociations;
    }

    public void setLibraryQuantityAssociations(List<LibraryQuantityAssociation> libraryQuantityAssociations) {
        this.libraryQuantityAssociations = libraryQuantityAssociations;
    }

    public List<ObjectId> getUsers() {
        return users;
    }

    /**
     * Add a user who has borrowed a book
     *
     * @param userId  The ID of the borrower
     */
    public void addUser(ObjectId userId){
        users.add(userId);
    }

    public void setUsers(List<ObjectId> users) {
        this.users = users;
    }

    /**
     * Add an opinion to a book
     *
     * @param opinion The opinion to be added
     */
    public void addOpinion(Opinion opinion){
        opinions.add(opinion);
    }
}
