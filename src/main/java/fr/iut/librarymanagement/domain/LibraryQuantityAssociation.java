package fr.iut.librarymanagement.domain;

import org.bson.types.ObjectId;

/**
 * Represent an library, with its quantity of books
 */
public class LibraryQuantityAssociation {
    private ObjectId library;
    private int quantity;

    public LibraryQuantityAssociation() {
    }

    /**
     * Create a new association
     *
     * @param library The library {@link Library}
     * @param quantity The quantity of the book in the library
     */
    public LibraryQuantityAssociation(ObjectId library, int quantity) {
        this.library = library;
        this.quantity = quantity;
    }

    public ObjectId getLibrary() {
        return library;
    }

    public void setLibrary(ObjectId library) {
        this.library = library;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
