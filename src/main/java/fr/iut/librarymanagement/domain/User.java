package fr.iut.librarymanagement.domain;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Represent a user in base
 */
@Document(collection = "User")
public class User {
    /**
     * The ID of the user. Automatically generated
     */
    @Id
    private ObjectId id;
    private String firstname;
    private String lastname;
    private Double age;
    private List<ObjectId> libraries;

    public User() {
    }

    /**
     * Create a user
     *
     * @param firstname The firstname of the user
     * @param lastname The lastname of the user
     * @param age The age of the user
     * @param libraries  The libraries where the user is
     */
    public User(String firstname, String lastname, Double age, List<ObjectId> libraries) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;
        this.libraries = libraries;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Double getAge() {
        return age;
    }

    public void setAge(Double age) {
        this.age = age;
    }

    public List<ObjectId> getLibraries() {
        return libraries;
    }

    public void setLibraries(List<ObjectId> libraries) {
        this.libraries = libraries;
    }

    /**
     * Add a library where a user is
     *
     * @param libraryId The new library where the user is
     */
    public void addLibrary(ObjectId libraryId){
        libraries.add(libraryId);
    }
}
