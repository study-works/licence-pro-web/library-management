package fr.iut.librarymanagement.domain;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Represent a library in base
 */
@Document(collection = "Library")
public class Library {
    /**
     * The ID of the user. Automatically generated
     */
    @Id
    private ObjectId id;
    private String name;
    private Address address;

    public Library() {
    }

    /**
     * Create a library
     *
     * @param name The na me of the library
     * @param address The address of the library {@link Address}
     */
    public Library(String name, Address address) {
        this.name = name;
        this.address = address;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
