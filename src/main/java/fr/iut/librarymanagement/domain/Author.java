package fr.iut.librarymanagement.domain;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Represent an author in base
 */
@Document(collection = "Author")
public class Author {
    /**
     * The ID of the user. Automatically generated
     */
    @Id
    private ObjectId id;
    private String firstname;
    private String lastname;
    private String biography;

    public Author() {
    }

    /**
     * Create an Author
     *
     * @param firstname The firstname of the author (ex: George R. R.)
     * @param lastname The lastname of the author (ex: MARTIN)
     * @param biography The biography of the author
     */
    public Author(String firstname, String lastname, String biography) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.biography = biography;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }
}
