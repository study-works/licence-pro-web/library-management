package fr.iut.librarymanagement.domain;

/**
 * Represent an address, for the libraries
 */
public class Address {
    private int number;
    private String address;
    private int postal_code;
    private String city;

    public Address() {
    }

    /**
     * Create an address
     *
     * @param number The number of the address (ex: 5)
     * @param adress The address (ex: Rue Vercingétorix)
     * @param potal_code The postal or ZIP code (ex: 63000)
     * @param city The city (ex: Clermont-Ferrand)
     */
    public Address(int number, String adress, int potal_code, String city) {
        this.number = number;
        this.address = adress;
        this.postal_code = potal_code;
        this.city = city;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(int postal_code) {
        this.postal_code = postal_code;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
