package fr.iut.librarymanagement;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * Config of the project
 */
@EnableMongoRepositories
public class Config {
}
